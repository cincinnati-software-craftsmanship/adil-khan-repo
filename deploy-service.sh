#!/bin/sh

SERVICE_NAME="${SERVICE_NAME:-default}"

if docker service inspect $SERVICE_NAME;
  then
    echo "Updating service $SERVICE_NAME"
    docker service update --image $CI_REGISTRY_IMAGE:$CI_BUILD_REF $SERVICE_NAME
  else
    PORT="${PORT:-80}"
    DOMAIN="${DOMAIN:-local}"
    echo "Creating service $SERVICE_NAME at $SERVICE_NAME.$DOMAIN"
    docker service create --name $SERVICE_NAME --network frontends --replicas 3 \
      --label ingress=true --label ingress.dnsname=$SERVICE_NAME.$DOMAIN --label ingress.targetport=$PORT \
      $CI_REGISTRY_IMAGE:$CI_BUILD_REF
fi
