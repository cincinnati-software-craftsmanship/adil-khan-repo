# Same-day shipping HTML starter project

1. Clone the starter project
2. Create repo in [Gitlab cincinnati-software-craftsmanship group](https://gitlab.com/cincinnati-software-craftsmanship)
3. Register Gitlab runners to project
4. Refer to [same-day-shipping .gitlab-ci.yml](https://gitlab.com/cincinnati-software-craftsmanship/same-day-shipping/blob/master/.gitlab-ci.yml)
5. Create review app deploy
6. Create staging deploy
7. Create manual production deploy
